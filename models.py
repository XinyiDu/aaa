from django.contrib.auth.models import AbstractUser
from django.db import models


# Create your models here.

# 用户模型.
class User(AbstractUser):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=32, blank=True, null=True)
    school_name = models.CharField(max_length=64, blank=True, null=True)
    major = models.CharField(max_length=64, blank=True, null=True)
    student_id = models.CharField(max_length=32, blank=True, null=True)
    teacher_name = models.CharField(max_length=32, blank=True, null=True)
    teacher_email = models.CharField(max_length=64, blank=True, null=True)
    project_name = models.CharField(max_length=64, blank=True, null=True)
    project_start_time = models.DateTimeField(blank=True, null=True)
    project_end_time = models.DateTimeField(blank=True, null=True)
    chapters = models.CharField(max_length=255, blank=True, null=True)
    status = models.IntegerField(default=0)
    role = models.CharField(max_length=16, blank=False)


# 项目模型. 存储项目和文档链接
class Project(models.Model):
    id = models.IntegerField(primary_key=True)
    # 学生
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    # 论文链接
    word_url = models.CharField(max_length=255, null=True)
    # 项目链接
    project_url = models.CharField(max_length=255, null=True)
    # overleaf链接
    overleaf_url = models.CharField(max_length=255, null=True)


# 消息模型. 存储消息
class Message(models.Model):
    id = models.IntegerField(primary_key=True)
    # 发表者
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    # 内容
    content = models.TextField()
    # 发表时间
    publish_time = models.DateTimeField(auto_now_add=True)


class AnalyseHitory(models.Model):
    id = models.IntegerField(primary_key=True)
    # 发表者
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    # 分析内容,用来展示和明天对比
    data = models.JSONField()
    # 发表时间
    analyse_time = models.DateTimeField(auto_now_add=True)
