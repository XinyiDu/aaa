import datetime
import json
import os
import threading
import time
import uuid

from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect

# Create your views here.
from docx import Document

from account.common.result import success, fail
from account.models import User, Project, Message, AnalyseHitory
from account.util.doc_util import analysis_meeting, analysis_report, analysis_report_cron
from account.util.email_util import get_random_code, send_email_code

# 登陆验证装饰器
from account.util.gitlab import get_commits, download, analyse_project
from account.util.onedriver import OneDriveSharedFolder

analyse_content = {}


def check_login(func):
    def wrapper(req):
        if not req.user or req.user.id is None:
            return redirect('/')  # 如果登陆用户不存在,则跳转到欢迎页面
        else:
            return func(req)  # 如果已登录,则继续执行

    return wrapper


def welcome(request):
    role = request.GET.get('role')
    print(role)
    if not role:
        return render(request, 'welcome.html')

    if role == 'student' or role == 'teacher':
        request.session['role'] = role
        return redirect('/login_index')
    else:
        return HttpResponse('error')


def login_index(request):
    if request.method == 'POST':
        # 获取前端传入的邮箱和密码
        email = request.POST.get('email')
        password = request.POST.get('password')
        if not email or not password:
            return render(request, 'login.html', {'error': 'All input fields are required！'})
        # 验证密码
        user = authenticate(username=email, password=password)
        if user:
            # 验证成功,登陆用户
            login(request, user)
            role = request.user.role
            if role == 'teacher':
                return redirect('/teacher_home')
            else:
                return redirect('/student_home')
        else:
            return render(request, 'login.html', {'error': 'The username or password is incorrect'})

    return render(request, 'login.html')


def register_index(request):
    if request.method == 'POST':
        # 同上
        email = request.POST.get('email')
        password = request.POST.get('password')
        if not email or not password:
            return render(request, 'register.html', {'error': 'All input fields are required.'})
        else:
            # 查询用户
            user = User.objects.filter(username=email)
            if user:
                # 邮箱已注册为,返回错误
                return render(request, 'register.html', {'error': 'The email address has been registered.'})
            # 注册用户
            User.objects.create_user(username=email, password=password)
            # 获取随机验证码
            code = get_random_code(6)
            # 发送邮件
            if send_email_code(email, code) == 1:
                # 保存验证码
                request.session['code'] = code
                request.session['user'] = email
                role = request.session['role']
                if role == 'teacher':
                    return redirect('/teacher_next')
                else:
                    return redirect('/student_next')
            else:
                return render(request, 'register.html', {'error': 'Failed to send the email verification code.'})

    return render(request, 'register.html')


def student_next(request):
    if request.method == 'POST':
        code_form = request.POST.get('code')
        email_form = request.POST.get('email')
        code = request.session['code']
        email = request.session['user']
        users = User.objects.filter(username=email)
        print('users:')
        print(users)
        if users:
            # 判断验证码是否正确
            if code == code_form:
                user = users[0]
                users = User.objects.filter(username=email_form, role='teacher')
                if users:
                    # 更新用户信息,激活账户
                    user.teacher_email = email_form
                    user.role = 'student'
                    user.status = 1
                    user.save()
                    return redirect('/login_index')
                else:
                    return render(request, 'student_next.html', {'error': 'The verification code is incorrect'})
            else:
                return render(request, 'student_next.html', {'error': 'The verification code is incorrect'})
        return render(request, 'student_next.html', {'error': 'The teacher does not exist'})

    return render(request, 'student_next.html')


def teacher_next(request):
    # 逻辑同上
    if request.method == 'POST':
        code_form = request.POST.get('code')
        code = request.session['code']
        email = request.session['user']
        users = User.objects.filter(username=email)
        if users:
            if code == code_form:
                user = users[0]
                user.role = 'teacher'
                user.status = 1
                user.save()
                return redirect('/login_index')
            else:
                return render(request, 'teacher_next.html', {'error': 'The verification code is incorrect'})
    return render(request, 'teacher_next.html')


def forget_pwd(request):
    if request.method == 'GET':
        return render(request, 'forget_pwd.html')
    if request.method == 'POST':
        code = request.session['code']
        email = request.session['email']

        code_form = request.POST.get('code')
        email_form = request.POST.get('email')
        password_form = request.POST.get('password')
        if not code_form or not email_form or not password_form:
            return render(request, 'forget_pwd.html', {'error': 'All input fields are required.'})
        users = User.objects.filter(username=email)
        if users:
            # 验证验证码
            if code == code_form:
                user = users[0]
                # 更新密码
                user.set_password(password_form)
                user.save()
                return redirect('/login_index')
            else:
                return render(request, 'forget_pwd.html', {'error': 'The verification code is incorrect'})
        else:
            return render(request, 'forget_pwd.html', {'error': 'The user does not exist'})


def user_index(request):
    pass


def index(request):
    return render(request, 'index.html')


def register(request):
    pass


def send_email(request):
    email = request.GET.get('email')
    print(email)
    if not email:
        return JsonResponse(fail('The email is required'))
    # 获取随机码
    code = get_random_code(6)
    print(code)
    if send_email_code(email, code) == 1:

        request.session['code'] = code
        request.session['email'] = email
        return JsonResponse(success('success', ''))
    else:
        return JsonResponse(fail('fail'))


def logout1(request):
    # 注销用户
    logout(request)
    return redirect('/login_index')


@check_login
def student_home(request):
    global analyse_content
    if request.method == 'GET':
        # 获取登陆用户对应项目信息
        projects = Project.objects.filter(user_id=request.user)
        git_url = ''
        onedrive_url = ''
        overleaf_url = ''
        if projects:
            git_url = projects[0].project_url
            onedrive_url = projects[0].word_url
            overleaf_url = projects[0].overleaf_url
        print(analyse_content)
        # 返回项目信息到前端
        return render(request, 'student/Home.html',
                      context={'git_url': git_url, 'onedrive_url': onedrive_url, 'overleaf_url': overleaf_url,
                               'analyse_content': analyse_content})
    if request.method == 'POST':
        # 获取前端传入参数
        type = request.GET.get('type')
        if type == "report":
            report_url = request.POST.get('report_url')
            if not report_url:
                print('未更新项目信息')
                #         user_id = models.ForeignKey(User,on_delete=models.CASCADE)
                # word_url = models.CharField(max_length=255, null=True)
                # project_url = models.CharField(max_length=255, null=True)
                # overleaf_url = models.CharField(max_length=255, null=True)

                return redirect('/student_home')
            print('更新项目信息')
            projects = Project.objects.filter(user_id=request.user)
            if projects:
                pro = projects[0]
                pro.word_url = report_url
                # 更新项目信息
                pro.save()
            else:
                p = Project(user_id=request.user, word_url=report_url)
                p.save()
            return redirect('/student_home')
        if type == "git":
            git_url = request.POST.get('git_url')
            if not git_url:
                print('未更新项目信息')
                #         user_id = models.ForeignKey(User,on_delete=models.CASCADE)
                # word_url = models.CharField(max_length=255, null=True)
                # project_url = models.CharField(max_length=255, null=True)
                # overleaf_url = models.CharField(max_length=255, null=True)

                return redirect('/student_home')
            print('更新项目信息')
            projects = Project.objects.filter(user_id=request.user)
            if projects:
                pro = projects[0]
                pro.project_url = git_url
                # 更新项目信息
                pro.save()
            else:
                p = Project(user_id=request.user, project_url=git_url)
                p.save()
            return redirect('/student_home')
        if type == "meeting":
            meeting_url = request.POST.get('meeting_url')
            if not meeting_url:
                print('未更新项目信息')
                #         user_id = models.ForeignKey(User,on_delete=models.CASCADE)
                # word_url = models.CharField(max_length=255, null=True)
                # project_url = models.CharField(max_length=255, null=True)
                # overleaf_url = models.CharField(max_length=255, null=True)

                return redirect('/student_home')
            print('更新项目信息')
            projects = Project.objects.filter(user_id=request.user)
            if projects:
                pro = projects[0]
                pro.overleaf_url = meeting_url

                # 更新项目信息
                pro.save()
            else:
                p = Project(user_id=request.user, overleaf_url=meeting_url)
                p.save()
            return redirect('/student_home')


@check_login
def student_announcement(request):
    if request.method == 'GET':
        user = request.user
        messages = None
        if user.role == 'student':
            users = User.objects.filter(username=user.teacher_email)
            # 获取所有message
            messages = Message.objects.filter(user_id=users[0]).order_by('-publish_time')
        messages = Message.objects.order_by('-publish_time').all()
        rs = []
        for mes in messages:
            rs.append({
                'username': mes.user_id.name,
                'content': mes.content,
                # 格式化时间
                'publish_time': mes.publish_time.strftime("%Y-%m-%d %H:%M:%S")

            })

        return render(request, 'student/Announcement.html', context={'messages': rs})


def student_home_by_teacher(request):
    if request.method == 'GET':
        if request.user.role != 'teacher':
            return HttpResponse('No access permission')
        id = request.GET.get('id')
        users = User.objects.filter(id=id)
        projects = Project.objects.filter(user_id=users[0])
        git_url = ''
        onedrive_url = ''
        overleaf_url = ''
        if projects:
            git_url = projects[0].project_url
            onedrive_url = projects[0].word_url
            overleaf_url = projects[0].overleaf_url
        return render(request, 'teacher/StudentHome.html',
                      context={'git_url': git_url, 'onedrive_url': onedrive_url, 'overleaf_url': overleaf_url,
                               'user': users[0]})


def student_announcement_by_teacher(request):
    if request.method == 'GET':
        if request.user.role != 'teacher':
            return HttpResponse('No access permission')
        id = request.GET.get('id')
        users = User.objects.filter(id=id)
        user = users[0]
        # 获取所有message
        messages = Message.objects.order_by('-publish_time').all()
        rs = []
        for mes in messages:
            rs.append({
                'username': mes.user_id.name,
                'content': mes.content,
                # 格式化时间
                'publish_time': mes.publish_time.strftime("%Y-%m-%d %H:%M:%S")

            })

        return render(request, 'teacher/StudentAnnouncement.html', context={'messages': rs, 'user': user})


chapters = ['Abstract', 'Introduction', 'Literature review', 'Background',
            'Analysis and Specification', 'Related technology introduction',
            'Design', 'Implement', 'Project management', 'Test', 'Evaluate',
            'Conclusion', 'Reference', 'Overview', 'Literature Review', 'Keywords', 'Acknowledgment'
            ]


def student_account_by_teacher(request):
    if request.method == 'GET':
        if request.user.role != 'teacher':
            return HttpResponse('No access permission')
        id = request.GET.get('id')
        users = User.objects.filter(id=id)
        count = 0
        user = users[0]
        chapters_data = []
        if user.chapters:
            print('!!!!!')
            print('user.chapters:', user.chapters)
            chapters_split = user.chapters.split(',')

            for i in chapters:
                # 如果chapter在数据库记录中,则表示chapter已被check
                if i in chapters_split:
                    chapters_data.append({'value': i, 'checked': True})
                    # 统计checed的chapter数据
                    count = count + 1
                else:
                    chapters_data.append({'value': i, 'checked': False})
        else:
            for i in chapters:
                chapters_data.append({'value': i, 'checked': False})
        print('??????')
        print(chapters_data)

        teacher_name = ''
        if user.teacher_email:
            users = User.objects.filter(username=user.teacher_email)
            if users:
                teacher_name = users[0].name

        return render(request, 'teacher/StudentAccount.html',
                      {'chapters': chapters_data, 'teacher_name': teacher_name, 'count': count, 'user': user})


@check_login
def student_account(request):
    global chapters
    if request.method == 'GET':
        count = 0
        user = request.user
        chapters_data = []
        if user.chapters:
            print(user.chapters)
            chapters_split = user.chapters.split(',')
            print(chapters)

            for i in chapters:
                # 如果chapter在数据库记录中,则表示chapter已被check
                if i in chapters_split:
                    chapters_data.append({'value': i, 'checked': True})
                    # 统计checed的chapter数据
                    count = count + 1
                else:
                    chapters_data.append({'value': i, 'checked': False})
        else:
            for i in chapters:
                chapters_data.append({'value': i, 'checked': False})
        print(chapters_data)

        teacher_name = ''
        if request.user.teacher_email:
            users = User.objects.filter(username=request.user.teacher_email)
            if users:
                teacher_name = users[0].name

        return render(request, 'student/Account.html',
                      {'chapters': chapters_data, 'teacher_name': teacher_name, 'count': count})
    if request.method == 'POST':
        type = request.GET.get('type')
        # 修改个人信息
        if type == 'personal':
            name = request.POST.get("name")
            teacher_email = request.POST.get("teacher_email")
            school_name = request.POST.get("school_name")
            student_id = request.POST.get("student_id")
            major = request.POST.get("major")
            if not name or not teacher_email or not school_name or not student_id or not major:
                print('/student_account')
                return redirect('/student_account')
            user = request.user
            user.name = name
            user.teacher_email = teacher_email
            user.school_name = school_name
            user.student_id = student_id
            user.major = major
            user.save()
            return redirect('/student_account')
        # 修改项目信息
        if type == 'project':
            project_name = request.POST.get("project_name")
            project_start_time = request.POST.get("project_start_time")
            project_end_time = request.POST.get("project_end_time")

            if not project_name or not project_start_time or not project_end_time:
                print('/student_account')
                return redirect('/student_account')
            user = request.user
            user.project_name = project_name
            user.project_start_time = datetime.datetime.strptime(project_start_time, "%Y-%m-%d")
            user.project_end_time = datetime.datetime.strptime(project_end_time, "%Y-%m-%d")
            user.save()
            return redirect('/student_account')
        # 修改chapters信息
        if type == 'chapters':
            # chapters = request.POST.get("chapters")
            user = request.user
            user.chapters = request.POST.get("chapters")
            user.save()
            return redirect('/student_account')


@check_login
def teacher_home(request):
    # 获取绑定自己的学生信息
    users = User.objects.filter(teacher_email=request.user.username)
    return render(request, 'teacher/Home.html', context={'users': users})


def teacher_announcement(request):
    # 同上
    if request.method == 'GET':
        # 获取所有message
        messages = Message.objects.order_by('-publish_time').all()
        rs = []
        # 循环构建dict放入数组中
        for mes in messages:
            rs.append({
                'username': mes.user_id.name,
                'content': mes.content,
                'publish_time': mes.publish_time.strftime("%Y-%m-%d %H:%M:%S")

            })

        return render(request, 'teacher/Announcement.html', context={'messages': rs})
    if request.method == 'POST':
        # 接受message内容
        content = request.POST.get('content')
        # 创建message
        Message.objects.create(user_id=request.user, content=content)
        return redirect('/teacher_announcement')


@check_login
def teacher_account(request):
    if request.method == 'GET':
        # 获取绑定自己的学生信息
        users = User.objects.filter(teacher_email=request.user.username)
        return render(request, 'teacher/Account.html', context={'users': users})
    if request.method == 'POST':
        # 获取前端传入参数
        name = request.POST.get("name")
        email = request.POST.get("email")
        school = request.POST.get("school")
        if not name or not email or not school:
            return render(request, 'teacher/Account.html', context={'error': 'All input fields are required！'})
        # 更新信息
        user = request.user
        user.name = name
        user.email = email
        user.username = email
        user.school_name = school
        user.save()
        return redirect('/teacher_account')


@check_login
def analyse_cache(request):
    type = request.GET.get('type')
    if type == 'code':
        rs = analyse_content.get('code')
        if not rs:
            rs = {}

        return JsonResponse(rs, safe=False)
    if type == 'meeting':
        rs = analyse_content.get('meeting')
        if not rs:
            rs = {}

        return JsonResponse(rs)


@check_login
def analyse(request):
    global analyse_content
    # 获取分析类型 report or code or meeting_record
    type = request.GET.get('type')
    user = request.user
    # 如果调用者是老师
    if request.user.role == 'teacher':
        id = request.GET.get('s_id')
        users = User.objects.filter(id=id)
        user = users[0]
    if type == 'report':
        # 从数据库中获取链接
        projects = Project.objects.filter(user_id=user)
        users = User.objects.filter(id=user.id)
        # user_chapters = users[0].chapters.split(',')
        word_url = projects[0].word_url
        # 下载meeting record
        o = OneDriveSharedFolder(word_url)

        filename = uuid.uuid4().hex
        print(filename + '.docx')
        # customize to your liking
        path = 'tmp' + os.sep + 'doc' + os.sep + filename + '.docx'
        o.download(path=path, concurrent_reqs=10)
        print('analysis_report_cron')
        # print(analysis_report_cron(path))

        suggestion = analysis_report(path)
        os.remove(path)
        analyse_content['report'] = {
            'data': suggestion
        }

        return JsonResponse({
            'data': suggestion
        })
    if type == 'code':
        # 调用gitlab api 获取提交记录和代码量(行数)
        projects = Project.objects.filter(user_id=user)
        project_url = projects[0].project_url
        project_data = project_url.split('/')

        rand_uuid = uuid.uuid4().hex
        path = 'tmp' + os.sep + 'code' + os.sep + rand_uuid + '.tar.zip'
        print(path)
        download(project_data[-2], project_data[-1], path)
        # code_result = analyse_project(path)
        # print(code_result)
        # os.remove(path)

        print(project_data)

        data, total = get_commits(project_data[-2], project_data[-1])
        # print(data, total)
        rs = {}
        commits = []
        group_num = len(data) // 12
        y = len(data) % 12
        x_data = []
        y_data = []
        j = 0
        code_num = 0
        for i in data[::-1]:
            j += 1
            commits.append({
                'message': i['message'],
                'date': i['committed_date'],
                'stats': [i['stats']['additions'], i['stats']['deletions']],
                'code_num': i['stats']['additions'] - i['stats']['deletions']
            })
            code_num += i['stats']['additions'] - i['stats']['deletions']

            if j == len(data):
                d = i['committed_date']
                x_data.append(d[0:4] + '-' + d[5:7] + '-' + d[8:10])
                y_data.append(code_num)

            if group_num == 0 or (j // group_num > 0 and j % group_num == 0):
                if len(x_data) == 11:
                    continue
                d = i['committed_date']
                # x_data.append(d[0:4] + '-' + d[5:7] + '-' + d[8:10] + ' ' + d[11:13] + ':' + d[14:16] + ':' + d[17:19])
                x_data.append(d[0:4] + '-' + d[5:7] + '-' + d[8:10])
                y_data.append(code_num)

        rs['commits'] = commits
        rs['total'] = total
        rs['chart_data'] = {
            'x': x_data,
            'y': y_data
        }
        # print(rs)
        analyse_content['code'] = rs

        return JsonResponse(rs, safe=False)
    if type == 'meeting':
        # 从数据库中获取链接
        projects = Project.objects.filter(user_id=user)
        meeting_record_url = projects[0].overleaf_url
        # 下载meeting record
        o = OneDriveSharedFolder(meeting_record_url)

        filename = uuid.uuid4().hex
        print(filename + '.docx')
        # customize to your liking
        path = 'tmp' + os.sep + 'doc' + os.sep + filename + '.docx'
        o.download(path=path, concurrent_reqs=10)
        suggestion = analysis_meeting(path)
        os.remove(path)

        analyse_content['meeting'] = {
            'data': suggestion
        }

        return JsonResponse({
            'data': suggestion
        })


@check_login
def analyse_his(request):
    user = request.user
    histories = AnalyseHitory.objects.filter(user=user).order_by('analyse_time')
    rs = []
    for his in histories:
        rs.append({
            'id': his.id,
            'data': his.data,
            'analyse_time': his.analyse_time.strftime('%Y-%m-%d %H:%M:%S')
        })
    return JsonResponse(rs, safe=False)


def analyse_everyday():
    while True:
        try:
            if datetime.datetime.now().hour == 0 and datetime.datetime.now().minute == 0:
                # if datetime.datetime.now().hour == 0 and datetime.datetime.now().minute == 0:
                # 下载项目代码和论文, 和前一天对比
                users = User.objects.filter(role='student')
                report_data = {}
                project_data = {}
                for user in users:
                    # 从数据库中获取链接
                    projects = Project.objects.filter(user_id=user)
                    users = User.objects.filter(id=user.id)
                    word_url = projects[0].word_url
                    # 下载论文.
                    o = OneDriveSharedFolder(word_url)

                    filename = uuid.uuid4().hex
                    print(filename + '.docx')
                    # customize to your liking
                    path = 'tmp' + os.sep + 'doc' + os.sep + filename + '.docx'
                    o.download(path=path, concurrent_reqs=10)
                    # 分析数据: 1. 章节数 2. 章节内容
                    result = analysis_report_cron(path)
                    os.remove(path)

                    # 将result写入数据库

                    # 下载项目代码并解压,遍历整个项目文件夹. 分析数据: {'files':['/mamage.py','/a','/a/a.txt'],'text':{
                    # '/mamage.py':
                    # }}
                    project_url = projects[0].project_url
                    urls = project_url.split('/')
                    print(urls)

                    rand_uuid = uuid.uuid4().hex
                    path = 'tmp' + os.sep + 'code' + os.sep + rand_uuid + '.tar.zip'
                    print(filename)
                    download(urls[-2], urls[-1], path)
                    code_result = analyse_project(path)
                    print(code_result)
                    os.remove(path)

                    # 获取前一天result,与之对比
                    histories = AnalyseHitory.objects.filter().order_by('-analyse_time')

                    if histories:
                        history = histories[0]
                        last_data = history.data
                        print('last_data', last_data)
                        k_arr = []
                        for k, v in last_data['report_data']['chapters'].items():
                            k_arr.append(k)
                        rs_k_arr = []
                        for k, v in result['chapters'].items():
                            rs_k_arr.append(k)
                        # 获取result中有而前一天没有的chapter, 也就是获取新增的
                        new_k_arr = list(set(rs_k_arr).difference(set(k_arr)))
                        content = ''
                        for k in new_k_arr:
                            content = content + "New part {} :{} words.\n".format(k, result['chapters'][k])
                        # 获取前一天中有而今天没有的chapter, 也就是获取删除的
                        del_k_arr = list(set(k_arr).difference(set(rs_k_arr)))
                        for k in del_k_arr:
                            content = content + "Delete part {} :{} words.\n".format(k, last_data['chapters'][k])
                        # 获取并集
                        union_k_arr = list(set(k_arr).union(set(rs_k_arr)))
                        for k in union_k_arr:
                            if result['chapters'][k] > last_data['report_data']['chapters'][k]:
                                content = content + "Add {} words to part {}\n".format(
                                    result['chapters'][k] - last_data['report_data']['chapters'][k]
                                    , k)
                            if result['chapters'][k] < last_data['report_data']['chapters'][k]:
                                content = content + "Delete {} words from part {}\n".format(
                                    last_data['report_data']['chapters'][k] - result['chapters'][k]
                                    , k)
                        if result['wx'] > last_data['report_data']['wx']:
                            content = content + "{} new references were added to the reference section.\n".format(
                                result['wx'] - last_data['report_data']['wx'])
                        if result['wx'] < last_data['report_data']['wx']:
                            content = content + "{}  references were deleted to the reference section.\n".format(
                                last_data['report_data']['wx'] - result['wx'])
                        result['content'] = content
                        report_data = result

                        # 项目代码分析(有历史记录条件下)
                        last_files = last_data['project_data']['files']
                        last_files_lines = last_data['project_data']['files_lines']

                        code_result['files']

                        new_k_arr = list(set(code_result['files']).difference(set(last_files)))
                        content = ''
                        for k in new_k_arr:
                            if code_result['files_lines'].get(k):
                                content = content + "New file:{} ({} lines).\n".format(k, code_result['files_lines'][k])
                            else:
                                content = content + "New file:{}.\n".format(k)
                        # 获取前一天中有而今天没有的chapter, 也就是获取删除的
                        del_k_arr = list(set(last_files).difference(set(code_result['files'])))
                        for k in del_k_arr:
                            if last_files_lines.get(k):
                                content = content + "Deleted file:{} ({} lines).\n".format(k, last_files_lines[k])
                            else:
                                content = content + "Delete file:{}.\n".format(k)
                        # 获取并集
                        union_k_arr = list(set(last_files).union(set(code_result['files'])))
                        for k in union_k_arr:
                            if code_result['files_lines'].get(k):
                                if code_result['files_lines'][k] > last_files_lines[k]:
                                    content = content + "Changed file: {} ({} new lines of code added)\n".format(
                                        k,
                                        code_result['files_lines'][k] - last_files_lines[k]
                                    )
                                if code_result['files_lines'][k] < last_files_lines[k]:
                                    content = content + "Changed file: {} ({} new lines of code deleted)\n".format(
                                        k,
                                        last_files_lines[k] - code_result['files_lines'][k])

                        code_result['content'] = content
                        project_data = code_result


                    else:
                        content = ''
                        for k, v in result['chapters'].items():
                            content = content + "New part {} :{} words.\n".format(k, v)

                        result['content'] = """
{} new references were added to the reference section.
{}
                        """.format(result['wx'], content)
                        print(result)
                        report_data = result

                        content = 'The following files have been added:\n'
                        # code_result['files_lines']
                        for f in code_result['files']:
                            count = code_result['files_lines'].get(f)
                            if not count:
                                content = content + '{}\n'.format(f)
                            content = content + '{} (New line {})\n'.format(f, count)

                        code_result['content'] = content
                        project_data = code_result
                    final_data = {
                        'project_data': project_data,
                        'report_data': report_data
                    }
                    print(final_data)
                    h = AnalyseHitory(user=user, data=final_data)
                    h.save()

                # loca_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                # print('本地时间：' + str(loca_time))
                # time.sleep(1)
                time.sleep(60)
        except Exception as e:
            print('发生错误，错误信息为：', e)
            time.sleep(60)
            continue


'''
主函数，用于启动所有定时任务，因为当前定时任务是手动实现，因此可以自由发挥
'''
try:
    # 启动定时任务，多个任务时，使用多线程
    task1 = threading.Thread(target=analyse_everyday)
    task1.start()
except Exception as e:
    print('发生异常：%s' % str(e))
